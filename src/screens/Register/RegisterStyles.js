import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    introText: {
        textAlign:'center',
        marginTop:10,
    },
    errorText: {
        color: "red",
        textAlign:'center',
        marginTop: 10,
        marginRight: 10,
        marginLeft: 10
    },
    inputField: {
        marginTop: 10,
        marginRight: 10,
        marginLeft: 10
    },
    loginButton: {
        backgroundColor: 'blue',
        borderColor:'blue',
        textAlign: 'center',
        marginTop: 10,
        marginRight: 10,
        marginLeft: 10
    },
    loginButtonPress: {
        backgroundColor: 'red',
    },
    signUpButton: {
        backgroundColor: 'green',
        borderColor:'green',
        textAlign: 'center',
        marginTop: 10,
        marginRight: 10,
        marginLeft: 10
    }
})