import React from 'react';

import { Text, Icon, Input, Button } from '@ui-kitten/components';
import RegisterStyles from './RegisterStyles';

// api here
import AuthRequest from '../../api/auth.api';

const renderIcon = (style) => (
    <Icon {...style} name={'eye'} />
);
function RegisterScreen(props) {
    const [error, setError] = React.useState(null)

    const [user, setUser] = React.useState({
        name: null,
        username: null,
        password: null,
        name: null
    })

    const onPressRegister = () => {
        AuthRequest.registerRequest(user)
            .then(res => {
                // console.log(JSON.stringify(res.data, 0, 2));
                props.navigation.navigate('Login');

            })
            .catch(err => {
                // console.log(JSON.stringify(err.response.data, 0, 2));
                setError(err.response.data.message)
            })
    }

    return (
        <>
            <Text style={RegisterStyles.introText}>Register Screen. </Text>
            <Input
                placeholder='Your Name'
                autoCapitalize = 'none'
                onChangeText={value => setUser({
                    ...user,
                    name: value
                })}
                defaultValue={user.name}
                style={RegisterStyles.inputField}
            />
            <Input
                placeholder='User Name'
                autoCapitalize = 'none'
                onChangeText={value => setUser({
                    ...user,
                    username: value
                })}
                defaultValue={user.username}
                style={RegisterStyles.inputField}
            />
            <Input
                secureTextEntry={true}
                placeholder='Password'
                autoCapitalize = 'none'
                onChangeText={value => setUser({
                    ...user,
                    password: value
                })}
                icon={renderIcon}
                defaultValue={user.password}
                style={RegisterStyles.inputField}
            />
            <Text style={RegisterStyles.errorText}>{error}</Text>
            <Button onPress={onPressRegister} style={RegisterStyles.signUpButton}>Register Account</Button>

        </>
    )
}

export default RegisterScreen;