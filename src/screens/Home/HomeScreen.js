import React from 'react';
import { Text, Button, Layout } from '@ui-kitten/components';

import Counter from '../../components/Counter/Counter';

// api here
import AuthRequest from '../../api/auth.api';
import RegisterStyles from '../Register/RegisterStyles';


class HomeScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            userLoggedIn: null
        }
    }

    async componentDidMount() {
        const userInfo = await AuthRequest.getUserInfo();
        this.setState({
            userLoggedIn: userInfo
        })
    }

    onPressLogout = () => {
        AuthRequest.logoutUser()
            .then(res => {
                // console.log(JSON.stringify(res.data, 0, 2));
                this.props.navigation.navigate('Login');

            })
    }

    render() {
        return (
            <>
                <Layout style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text category="h3">Hello {this.state.userLoggedIn}!</Text>
                    <Button onPress={this.onPressLogout}>Log Out</Button>
                </Layout>

                <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Counter initValue={4} helperText="abac" />
                </Layout>
            </>

        )
    }
}

export default HomeScreen;