import React from 'react';
import { Text, Icon, Input, Button } from '@ui-kitten/components';

import LoginStyles from './LoginStyles';

// api here
import AuthRequest from '../../api/auth.api';

const renderIcon = (style) => (
    <Icon {...style} name={'eye'} />
);

function LoginScreen(props) {
    const [error, setError] = React.useState(null)
    const [username, setUsername] = React.useState(null)
    const [password, setPassword] = React.useState(null)

    React.useEffect(() => {
        checkingUserInfo();
    }, [])

    const checkingUserInfo = async () => {
        const user = await AuthRequest.getUserInfo();
        if (user) {
            props.navigation.navigate('Home');
        }
    }

    const onPressLogin = () => {

        AuthRequest.loginRequest({
            username, 
            password
        })
            .then(async res => {
                setError(null)
                await AuthRequest.setUserInfo(res.data.user.username);
                props.navigation.navigate('Home');

            })
            .catch(err => {
                // console.log(JSON.stringify(err.response.data, 0, 2));
                setError(err.response.data.message)
            })
    }

    const onPressRegister = () => {
        props.navigation.navigate('Register');
    }

    return (
        <>
            <Text style={LoginStyles.welcomeText}>Hello! Welcome. </Text>
            <Input
                placeholder='User Name'
                autoCapitalize = 'none'
                onChangeText={setUsername}
                defaultValue={username}
                style={LoginStyles.inputField}
            />
            <Input
                secureTextEntry = {true}
                placeholder='Password'
                autoCapitalize = 'none'
                onChangeText={setPassword}
                icon={renderIcon}
                defaultValue={password}
                style={LoginStyles.inputField}
            />
            <Text style={LoginStyles.errorText}>{error}</Text>
            <Button onPress={onPressLogin} style={LoginStyles.loginButton}>Login</Button>
            <Button onPress={onPressRegister} style={LoginStyles.signUpButton}>Register Account</Button>

        </>
    )
}

export default LoginScreen;