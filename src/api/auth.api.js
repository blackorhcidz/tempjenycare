import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

function registerRequest(user) {
    return axios.post("http://35.182.252.31:3001/api/register", user);
}

function loginRequest(user) {
    return axios.post("http://35.182.252.31:3001/api/login", user);
}

function getUserInfo() {
    return AsyncStorage.getItem('username');
}

function setUserInfo(data) {
    return AsyncStorage.setItem('username', data);
}

function logoutUser() {
    return AsyncStorage.removeItem('username');
}

export default {
    getUserInfo,
    setUserInfo,
    logoutUser,
    registerRequest,
    loginRequest
}

export {
    getUserInfo,
    setUserInfo,
    logoutUser,
    registerRequest,
    loginRequest
}