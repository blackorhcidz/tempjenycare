import React from "react";

import { Text } from '@ui-kitten/components';
import CounterControl from './CounterControl';
import CounterStyles from './CounterStyles';


class Counter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            count: props.initValue
        }
    }

    decreaseCounter = () => {
        this.setState({
            count: this.state.count - 1
        })
    }

    increaseCounter = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        return (
            <>
                {/* <Text style={CounterStyles.text} category='h6'>{this.props.helperText}</Text> */}
                <CounterControl type="decrease" onButtonPress={this.decreaseCounter} />
                <Text style={CounterStyles.text} category='h2'>{this.state.count}</Text>
                <CounterControl type="increase" onButtonPress={this.increaseCounter} />
            </>
        )
    }
}

export default Counter;