import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    text: {
        color: 'red',
        textAlign: 'center',
        margin: 20
    }
})