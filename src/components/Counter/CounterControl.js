import React from 'react';
import { Text, Icon, Button } from '@ui-kitten/components';

const PlusIcon = (style) => (
    <Icon name='plus' {...style} />
);

const MinusIcon = (style) => (
    <Icon name='minus' {...style} />
);

class CounterControl extends React.Component {

    render() {
        return (
            <Button onPress={this.props.onButtonPress} icon={
                this.props.type === "increase" ? PlusIcon : MinusIcon
            }></Button>
        )
    }
}

export default CounterControl;